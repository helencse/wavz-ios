//
//  Globals.swift
//  Wavz
//
//  Created by Helen Vasconcelos on 5/6/15.
//  Copyright (c) 2015 Helen Vasconcelos. All rights reserved.
//

import UIKit

class Globals: NSObject {
    let didUpdateLocationKey = "didUpdateLocation"
    // load a view
    class func loadTopNib( name : String, owner : AnyObject! ) -> UIView {
        return NSBundle.mainBundle().loadNibNamed(name, owner: owner, options: nil)[0] as! UIView
    }
}
