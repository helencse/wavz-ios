//
//  Gateway.swift
//  Wavz
//
//  Created by Helen Vasconcelos on 5/5/15.
//  Copyright (c) 2015 Helen Vasconcelos. All rights reserved.
//

import UIKit

class Gateway: NSObject {
    private let apiURL = "http://localhost:8888/wavz/api"
    
    // Get SPOTS
    func getSpots( latitude lat: Double, longitude lon: Double, distance: Double,
        success:(spots:[Spot]) -> Void,
        failure:(error: NSError!) -> Void
        )
    {
        var spots = [Spot]()
        let getSpotsURL = String(format: "\(apiURL)/getSpots.php")
        
        let params = ["latitude" : lat,
                      "longitude" : lon,
                      "distance" : distance
        ]
        
        let manager = AFHTTPRequestOperationManager()
        
        manager.GET(getSpotsURL, parameters: params,
            success: {
                (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                let jsonResponse = JSON(responseObject)
                if jsonResponse["success"].int == 1  {
                    let results = jsonResponse["results"]
                    for (key: String, subJson: JSON) in results {
                        spots.append( self.spotFromJSON(subJson) )
                        success(spots: spots)
                    }
                }
            },
            failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                println("An error occurred while trying to add a location")
                failure(error: error)
        })
    }
    
    // Add Spot
    func addSpot( #name: String, latitude lat: Double, longitude lon: Double, completed:(success:Bool) -> Void) {
        let addSpotURL = String(format: "\(apiURL)/addSpot.php")
        
        let manager = AFHTTPRequestOperationManager()
        
        let params = ["lat" : lat,
                      "lon" : lon,
                     "name" : name]
        
        manager.GET(addSpotURL, parameters: params,
            success: {
                (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                completed(success: true)
            },
            failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                println("An error occurred while trying to add a location")
                completed(success: false)
        })
       
    }
}


extension Gateway {
    func spotFromJSON( json : JSON ) -> Spot {
        var spot = Spot()
        //SID
        if let sid = json["sid"].intValue as Int! {
            spot.sid = sid
        }
        //NAME
        if let name = json["name"].string as String! {
            spot.name = name
        }
        //Latitude
        if let latitude = json["latitude"].doubleValue as Double! {
            spot.latitude = latitude
        }
        // Longitude
        if let longitude = json["longitude"].doubleValue as Double! {
            spot.longitude = longitude
        }
        return spot
        
    }
}