//
//  AddSpotViewController.swift
//  Wavz
//
//  Created by Helen Vasconcelos on 5/6/15.
//  Copyright (c) 2015 Helen Vasconcelos. All rights reserved.
//

import UIKit
import MapKit

class AddSpotViewController: UIViewController {

    @IBOutlet weak var spotName: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    var locationCoordinate : CLLocationCoordinate2D?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: Selector("dismissKeyboard"))
        self.view.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
        self.title = "Add Spot"
        
        self.mapView.delegate = self
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didUpdateLocation:", name: Globals().didUpdateLocationKey, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didUpdateLocation( notification: NSNotification ) {
        let userInfo:Dictionary<String,CLLocationManager!> = notification.userInfo as! Dictionary<String,CLLocationManager!>
        let manager : CLLocationManager = userInfo["manager"]!
        
        // Add pin to map
        let pin = MKPointAnnotation()
        pin.coordinate = manager.location.coordinate
        self.locationCoordinate = manager.location.coordinate
        self.mapView.addAnnotation(pin)
        self.mapView.region = MKCoordinateRegionMakeWithDistance(pin.coordinate, 1000, 1000)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Globals().didUpdateLocationKey, object: nil)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func addSpot(sender: AnyObject) {
        if !self.isFormValid() {
            return
        }
        Gateway().addSpot(name: self.spotName.text, latitude: self.locationCoordinate!.latitude, longitude: self.locationCoordinate!.longitude) { (success) -> Void in
            var alert : UIAlertView
            if success {
                alert = UIAlertView(title: "Success", message: "Your spot has been added successfuly", delegate: self, cancelButtonTitle: "OK")
            }
            else {
                alert = UIAlertView(title: "Error", message: "We weren't able to add your spot. Please try again.", delegate: self, cancelButtonTitle: "OK")
            }
            alert.show()
        }
    }
    
    func isFormValid() -> Bool {
        if self.spotName.text.isEmpty {
            let alert = UIAlertView(title: "Wait", message: "Please enter a spot name", delegate: self, cancelButtonTitle: "OK!")
            alert.show()
            return false
        }
        return true
    }
}

extension AddSpotViewController : CLLocationManagerDelegate, MKMapViewDelegate {
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
//        let pin = MKPointAnnotation()
//        pin.coordinate = manager.location.coordinate
//        self.locationCoordinate = manager.location.coordinate
//        self.mapView.addAnnotation(pin)
//        self.mapView.region = MKCoordinateRegionMakeWithDistance(pin.coordinate, 1000, 1000)
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Failed to get location ")
    }
}
