//
//  SpotsViewController.swift
//  Wavz
//
//  Created by Helen Vasconcelos on 5/11/15.
//  Copyright (c) 2015 Helen Vasconcelos. All rights reserved.
//

import UIKit
import MapKit

class SpotsViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var spots = [Spot]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Spots near you"
        
        self.tableView.hidden = true
        self.containerView.hidden = true
        // Add new spot button
        let addSpot : UIBarButtonItem = UIBarButtonItem(title: "+", style: UIBarButtonItemStyle.Done, target: self, action: Selector("addSpot"))
        self.navigationItem.rightBarButtonItem = addSpot
        self.navigationItem.rightBarButtonItems = [addSpot]
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didUpdateLocation:", name: Globals().didUpdateLocationKey, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addSpot() {
        let addSpotVC = AddSpotViewController(nibName: "AddSpotView", bundle: nil)
        self.navigationController?.pushViewController(addSpotVC, animated: true)
    }
    
    func didUpdateLocation( notification : NSNotification ) {
        let userInfo:Dictionary<String,CLLocationManager!> = notification.userInfo as! Dictionary<String,CLLocationManager!>
        let manager : CLLocationManager = userInfo["manager"]!
        
        Gateway().getSpots(latitude: manager.location.coordinate.latitude, longitude: manager.location.coordinate.longitude, distance: 60,
        success: { (spots) -> Void in
            if spots.count > 0 {
                self.spots = spots
                self.tableView.hidden = false
                self.containerView.hidden = true
                self.tableView.reloadData()
            }
            else {
                self.containerView.hidden = false
            }
            
        }, failure: { (error) -> Void in
            println("An error occured while grabbing spots")
        })
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Globals().didUpdateLocationKey, object: nil)
    }
}

extension SpotsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.spots.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "Cell"
        var cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath) as! UITableViewCell

        cell.textLabel?.text = self.spots[indexPath.row].name
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //aa
    }
}

extension SpotsViewController : CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        
       
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Failed to get location ")
    }
}
