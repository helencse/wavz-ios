//
//  WavzUser.swift
//  Wavz
//
//  Created by Helen Vasconcelos on 5/6/15.
//  Copyright (c) 2015 Helen Vasconcelos. All rights reserved.
//

import UIKit
import CoreLocation

let sharedUser = WavzUser()

class WavzUser: NSObject {
    var locationCoordinate : CLLocationCoordinate2D?
    var locationManager = CLLocationManager()
    
    override init() {
        super.init()
    }
    
    func getUserLocation() {
        // Get user's location
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
}

extension WavzUser : CLLocationManagerDelegate {
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        NSNotificationCenter.defaultCenter().postNotificationName(Globals().didUpdateLocationKey, object: self, userInfo: ["manager" : manager])
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Error retrieving location")
    }
}


