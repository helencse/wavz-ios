//
//  Spot.swift
//  Wavz
//
//  Created by Helen Vasconcelos on 5/11/15.
//  Copyright (c) 2015 Helen Vasconcelos. All rights reserved.
//

import UIKit

class Spot: NSObject {
    var latitude : Double?
    var longitude : Double?
    var name : String?
    var sid : Int?
    
    
    override init() {
        
    }
    
    convenience init( sid : Int, name : String, latitude lat : Double, longitude lon : Double ) {
        self.init()
        self.latitude = lat
        self.longitude = lon
        self.name = name
        self.sid = sid
    }
    
}
